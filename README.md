# README #



CS 426: Homework 4
RPM Repackaging
Background

Yersinia is a network attack security auditing tool. It is packaged for
Fedora, but not for CentOS 6. The goal is to repackage it for CentOS
6.3. Your package must disable GTK and ncurses support.

Compiling Yersinia on CentOS 6.3 without ncurses support requires
patching the source code. A patch is available at
http://sources.gentoo.org/cgi-bin/viewvc.cgi/gentoo-x86/net-analyzer/yersinia/files/yersinia-0.7.1-no-ncurses.patch.


Instructions

 1. Download the source RPM for the Fedora 17 yersinia package.
 2. Copy the source RPM to a Vagrant build CentOS machine with EPEL enabled.
 3. Modify the spec file so that GTK is disabled. This requires passing
    options to configure.
 4. Modify the spec file so that ncurses is disabled. This requires
    patching the source.
 5. Build an RPM
 6. Sign the RPM


    Deliverables

 1. A link to a Bitbucket repository containing your spec file.
 2. Put the signed RPM in the downloads area of your Bitbucket repository.
 3. Put your public key in the downloads area of your Bitbucket repository.


    Requirements

  * The RPM must install on CenOS 6.3 minimal (with EPEL enabled).
  * GTK support must be disabled in the spec file.
  * Ncurses support must be disabled in the spec file.
  * The linked patch must be applied in the spec file.
  * After installing the package, the yersinia command should run
    successfully.

Grade: 93%
Instructor Comment:
 
ncurses disabled w/ patch
	--enable-ncurses is not the right flag, this relies on the presence of ncurses-devel to enable/disable ncurses. The corrrect flag  
        is --without-ncurses.

My response:  I researched and found the --without-ncurses flag, and tried it with make config/make/install to no avail.  --enable-ncurses worked...