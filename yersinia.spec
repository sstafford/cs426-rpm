Name:           yersinia
Version:        0.7.1
Release:        8%{?dist}
Summary:        Network protocols tester and attacker

Group:          Applications/Internet
License:        GPLv2+
URL:            http://www.yersinia.net/
Source0:        http://www.yersinia.net/download/%{name}-%{version}.tar.gz
Patch0:         yersinia-0.7.1-no-ncurses.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  libpcap-devel
BuildRequires:  libnet-devel


%description
Yersinia is a network tool designed to take advantage of some weakeness
in different network protocols. It pretends to be a solid framework for
analyzing and testing the deployed networks and systems.

Currently, there are some network protocols implemented, but others are 
coming (tell us which one is your preferred). Attacks for the following
network protocols are implemented (but of course you are free for 
implementing new ones):

* Spanning Tree Protocol (STP)
* Cisco Discovery Protocol (CDP)
* Dynamic Trunking Protocol (DTP)
* Dynamic Host Configuration Protocol (DHCP)
* Hot Standby Router Protocol (HSRP)
* IEEE 802.1Q and IEEE 802.1X
* Inter-Switch Link Protocol (ISL)
* VLAN Trunking Protocol (VTP)


%prep
%setup -q
%patch0

%build
%configure --with-pcap-includes=%{_includedir}/pcap  --enable-gtk=false  --enable-ncurses=false 
make %{?_smp_mflags} CFLAGS="%{optflags}"

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} INSTALL="install -p"
# Convert to utf-8
for file in THANKS; do
    mv $file timestamp
    iconv -f ISO-8859-1 -t UTF-8 -o $file timestamp
    touch -r timestamp $file
done


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING FAQ README THANKS TODO
%{_mandir}/man?/%{name}.*
%{_bindir}/%{name}


%changelog
* Sat Jan 14 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.7.1-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Tue Dec 06 2011 Adam Jackson <ajax@redhat.com> - 0.7.1-7
- Rebuild for new libpng

* Sun Oct 04 2009 Kevin Kofler <Kevin@tigcc.ticalc.org> - 0.7.1-6
- Add --with-pcap-includes to fix build with libpcap 1.0

* Thu Sep 24 2009 Fabian Affolter <fabian@bernewireless.net> - 0.7.1-5
- Rebuild for new libpcap

* Mon Jul 27 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.7.1-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.7.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Fri Dec 26 2008 Fabian Affolter <fabian@bernewireless.net> - 0.7.1-2
- Added CFLAGS

* Thu Dec 23 2008 Fabian Affolter <fabian@bernewireless.net> - 0.7.1-1
- Initial spec for Fedora
